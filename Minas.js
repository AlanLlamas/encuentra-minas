var
	campo = document.getElementById('campo'),
	boton = document.getElementsByTagName('button'),
	lis = document.getElementsByClassName('cuadrante'),
	campoOriginal,
	reglasOriginal,
	Reglas = 	"<ol id = \"lista\">" 
					+ "<li>Da click a un cuadro.</li>"
					+ "<li>Si el cuadro es bomba pierdes, si el cuadro esta vacio, se abren los que esten al rededor.</li>"
					+ "<li>Si no contiene un numero no tiene bombas al rededo, Si contiene un numero, se abrira solo ese cuadro, hay una bomba cerca.</li>"
					+ "<li>Dando click derecho puedes colocar o quitar una bandera y evitar dar click a ese cuadro.</li>"
					+ "<li>Solo poniendo todas las banderas y abriendo todos los cuadros ganas.</li>"
				+"</ol>" ;
	facil = [

			1,1,1,1,1,1,1,1,1,
			1,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0
	],
	dificil = [

		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	],
	muyDificil = [

		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	],
	imposible = [

		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	];

//esta parte permite elegir la dificultad que se va a jugar.
	for (var i = 0; i < boton.length; i++) {
		boton[i].addEventListener("click",botton)
	};
	function botton () {
		dificultad = this.id;
		campoOriginal = campo.innerHTML;
		reglasOriginal = reglas.innerHTML
		
		campo.innerHTML = "";

		if (dificultad == "facil") {

			createArray(facil);

		};
		if (dificultad == "dificil") {

			createArray(dificil)

		};
		if (dificultad == "muyDificil") {

			createArray(muyDificil)
			
		};
		if (dificultad == "imposible") {

			createArray(imposible)
			
		};	
	}

function createArray (arr) {
	//acomoda un array de manera random en un array multidimensional y le da un data-value entre los valores del array randomizado.
	var
		result = [],
		len = arr.length;
		resultMulti = [];
		//en facil son 9 casillas
		f = [0,0,0,0,0,0,0,0,0];
		//en dificl son 16 casillas
		d = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	//esta es la parte que shuflea el array
		for (var i = 0; i < len; i++) {
			
			index = Math.floor( Math.random() * arr.length);
			result[i] = arr[index];
			arr.splice(index,1);


		};
	//esta es la parte que acomoda nuestro array en un array multidimensional, 
		if (arr == facil ) {
			len = f.length
		};
		if (arr == dificil || arr == muyDificil || arr == imposible) {
			len = d.length
		};
		resultMulti=[];
			
		for (var i = 0; i < len; i++) {

			resultado = [];
		
			for (var j = 0; j < len; j++) {

				resultado.push(result[0]);
				result.splice(0,1);
				resultMulti[i] = resultado ;
			
			};			
		};
				

				
				
	//esta parte crea los elementos que conforman la cuadricula y les añade un atributo con su posicion en x o en y
		for (var i = 0; i < len; i++) {
			if (arr == facil ) {
				campo.setAttribute('class', "facil")
			};
			if (arr == dificil || arr == muyDificil || arr == imposible) {
				campo.setAttribute('class', "dificil")
			};
			campo.innerHTML += "<ul id = \"" + (i + 1) +"\"></ul>";
			X = document.getElementById(i + 1);
			
			for (var j = 0; j < len; j++) {
				
				X.innerHTML += "<li id=\"" + i + "," + j + "\" class = \"cuadrante\" cont = \"0\" x = \"" + i + "\" y = \"" + j + "\"></li>";
				li = document.getElementById(i + "," + j)
			
		
			};

		};	
		reglas.innerHTML = Reglas;


	for (var i = 0; i < lis.length; i++) {
		list = lis[i];
		list.addEventListener('click', click)
		list.setAttribute("clicked",0)
		list.addEventListener('contextmenu', bandera)
		
		
	};
	bombas = [];
	for (var i = 0; i < resultMulti.length; i++) {
		resultMulti[i]
		for (var j = 0; j < resultMulti[i].length; j++) {
			if (resultMulti[i][j] == 1) {

				bomba = document.getElementById(i + "," + j)
				bombas.push([i,j])
					cerca(i,j)

			};
		};
	};



	function bandera () {

		if (this.style.background != "#E0E0E0") {
			//"<img src=\"bandera.jpg\">"
			if ( this.innerHTML == "") {
			this.innerHTML = "<img src=\"bandera.jpg\">";
			this.removeEventListener('click', click)
			this.setAttribute("clicked", 1);

			}else if (this.innerHTML == "<img src=\"bandera.jpg\">") {
				this.innerHTML = "";
				for (var i = 0; i < lis.length; i++) {
					list = lis[i];
					list.addEventListener('click', click);
					this.setAttribute("clicked",0);
				};

			};

		};

	}


	function click () {

		x = this.getAttribute("x");
		y = this.getAttribute("y");
		coordenadas = resultMulti[x][y];
		//console.log("estas en las coordenadas : " + x + "," + y)
		
		if (coordenadas == 1) {

			for (var i = 0; i < bombas.length; i++) {

				bombs = document.getElementById(bombas[i])				
				bombCont = bombs.innerHTML
				this.innerHTML = "<img src=\"ebomba.jpg\"/>";
				bombs.innerHTML = "<img src=\"bomba.jpg\"/>";
			
			};
			for (var i = 0; i < lis.length; i++) {
				lis[i].removeEventListener('click', click);
				lis[i].removeEventListener('contextmenu', bandera);
				
			};
			reglas.innerHTML = "<p id=\"perdedor\" >Perdiste</p>"
			setTimeout(cuenta, 3000)
			setTimeout(recargar, 8000)
			
		}else {

			this.style.background = "#E0E0E0";
			this.removeEventListener('click', click);
			this.removeEventListener('contextmenu', bandera);
			this.setAttribute("clicked", 1);
			EsteCont = this.getAttribute("cont");
			if (EsteCont != 0) {
				this.innerHTML = EsteCont;
			}else {
				cerca(x,y)
			}
			counter = 0;
			for (var i = 0; i < lis.length; i++) {
				if (lis[i].getAttribute("clicked") == 1) {

					counter += 1;

				};
				if (counter == lis.length) {
					for (var i = 0; i < lis.length; i++) {
						lis[i].removeEventListener('click', click);
						lis[i].removeEventListener('contextmenu', bandera);
						reglas.innerHTML = "<p id=\"ganador\" >Felicidades Ganaste!!!!</p>"
						setTimeout(cuenta, 3000)
						setTimeout(recargar, 8000)
						
					};


				};
				console.log(lis.length)
				console.log(counter)
			};


		}
	};

	function cuenta () {
				secs = "segundos";
				seg = 6;
			function segundo () {
				if (--seg > 0) {
					if (seg == 1) {
						secs = "segundo";
					};
				reglas.innerHTML = "<p id=\"texto\" >Espera " + seg + " " + secs + " para elegir dificultad. </p>";
			
					setTimeout(segundo, 1000);
				};	
			}
			segundo()

	}


	function recargar () {
		document.location.reload(true);
	}


	function cerca (X,Y) {

			x = Number(X);
			y = Number(Y);
		
		

			arriba = (x - 1) + "," + y;
			arribaDerecha = (x - 1) + "," + (y + 1 );
			derecha = x + "," + (y + 1);
			abajoDerecha = (x + 1) + "," + (y + 1);
			abajo = (x + 1) + "," + y;
			abajoIzquierda = (x +1) + "," + (y -1);
			izquierda = x + "," + (y - 1);
			arribaIzquierda = (x - 1) + "," + (y - 1)

			Aqui = document.getElementById(x + "," + y)
			Arriba = document.getElementById(arriba)
			ArribaDerecha = document.getElementById(arribaDerecha)
			Derecha = document.getElementById(derecha)
			AbajoDerecha = document.getElementById(abajoDerecha)
			Abajo = document.getElementById(abajo)
			AbajoIzquierda = document.getElementById(abajoIzquierda)
			Izquierda = document.getElementById(izquierda)
			ArribaIzquierda = document.getElementById(arribaIzquierda)
				
			//estos son los contadores "cont" van al rededor de las bombas.
			if (resultMulti[x][y] == 1) {

				if (Arriba != null && resultMulti[(x - 1)][y] != 1) {
						
						ArribaCont = Arriba.getAttribute("cont");

							
						if (ArribaCont == 7) {

							Arriba.setAttribute("cont", 8 );					
							

						};
						if (ArribaCont == 6) {

							Arriba.setAttribute("cont", 7 );										

						};
						if (ArribaCont == 5) {

							Arriba.setAttribute("cont", 6 );											

						};
						if (ArribaCont == 4) {

							Arriba.setAttribute("cont", 5 );											

						};
						if (ArribaCont == 3) {

							Arriba.setAttribute("cont", 4 );										

						};
						if (ArribaCont == 2) {

								Arriba.setAttribute("cont",3); 													

						};
						if ( ArribaCont == 1) {

								Arriba.setAttribute("cont",2); 								
						};
						if (ArribaCont == 0) {

								Arriba.setAttribute("cont",1);							
																	
						};

				}
				if (ArribaDerecha != null && resultMulti[(x - 1)][(y + 1 )] != 1){
							
						ArribaDerechaCont = ArribaDerecha.getAttribute("cont");

							
						if (ArribaDerechaCont == 7) {

							ArribaDerecha.setAttribute("cont", 8 )							

						};
						if (ArribaDerechaCont == 6) {

							ArribaDerecha.setAttribute("cont", 7 )							

						};
						if (ArribaDerechaCont == 5) {

							ArribaDerecha.setAttribute("cont", 6 )							

						};
						if (ArribaDerechaCont == 4) {

							ArribaDerecha.setAttribute("cont", 5 )							

						};
						if (ArribaDerechaCont == 3) {

							ArribaDerecha.setAttribute("cont", 4 )							

						};
						if (ArribaDerechaCont == 2) {

								ArribaDerecha.setAttribute("cont", 3 )								

						};
						if ( ArribaDerechaCont == 1) {

								ArribaDerecha.setAttribute("cont",2 )								
						};
						if (ArribaDerechaCont == 0) {

								ArribaDerecha.setAttribute("cont", 1)												

						};

				}		
				if (Derecha != null && resultMulti[x][(y + 1 )] != 1) {
							
						DerechaCont = Derecha.getAttribute("cont");

							
						if (DerechaCont == 7) {

							Derecha.setAttribute("cont", 8 )							

						};
						if (DerechaCont == 6) {

							Derecha.setAttribute("cont", 7 )							

						};
						if (DerechaCont == 5) {

							Derecha.setAttribute("cont", 6 )							

						};
						if (DerechaCont == 4) {

							Derecha.setAttribute("cont", 5 )							

						};
						if (DerechaCont == 3) {

							Derecha.setAttribute("cont", 4 )							

						};
						if (DerechaCont == 2) {

								Derecha.setAttribute("cont", 3 )								

						};
						if ( DerechaCont == 1) {

								Derecha.setAttribute("cont", 2 )								
						};
						if (DerechaCont == 0) {

								Derecha.setAttribute("cont", 1)												

						};

				} 	
				if (AbajoDerecha != null && resultMulti[(x + 1)][(y + 1 )] != 1) { 
							
						AbajoDerechaCont = AbajoDerecha.getAttribute("cont");

						if (AbajoDerechaCont == 7) {

							AbajoDerecha.setAttribute("cont", 8 )							

						};
						if (AbajoDerechaCont == 6) {

							AbajoDerecha.setAttribute("cont", 7 )							

						};
						if (AbajoDerechaCont == 5) {

							AbajoDerecha.setAttribute("cont", 6 )							

						};
						if (AbajoDerechaCont == 4) {

							AbajoDerecha.setAttribute("cont", 5 )							

						};
						if (AbajoDerechaCont == 3) {

							AbajoDerecha.setAttribute("cont", 4 )							

						};
						if (AbajoDerechaCont == 2) {

								AbajoDerecha.setAttribute("cont", 3 )								

						};
						if ( AbajoDerechaCont == 1) {

								AbajoDerecha.setAttribute("cont", 2 )								
						};
						if (AbajoDerechaCont == 0) {

								AbajoDerecha.setAttribute("cont", 1)												

						};


				}
				if (Abajo != null && resultMulti[(x + 1)][y] != 1){
							
						AbajoCont = Abajo.getAttribute("cont");

						if (AbajoCont == 7) {

							Abajo.setAttribute("cont", 8 )							

						};
						if (AbajoCont == 6) {

							Abajo.setAttribute("cont", 7 )							

						};
						if (AbajoCont == 5) {

							Abajo.setAttribute("cont", 6 )							

						};
						if (AbajoCont == 4) {

							Abajo.setAttribute("cont", 5 )							

						};
						if (AbajoCont == 3) {

							Abajo.setAttribute("cont", 4 )							

						};
						if (AbajoCont == 2) {

								Abajo.setAttribute("cont", 3 )								

						};
						if ( AbajoCont == 1) {

								Abajo.setAttribute("cont", 2 )								
						};
						if (AbajoCont == 0) {

								Abajo.setAttribute("cont", 1)												

						};
				}
				if (AbajoIzquierda != null && resultMulti[(x + 1)][(y - 1)] != 1) {
							
						AbajoIzquierdaCont = AbajoIzquierda.getAttribute("cont");

						if (AbajoIzquierdaCont == 7) {

							AbajoIzquierda.setAttribute("cont", 8 )							

						};
						if (AbajoIzquierdaCont == 6) {

							AbajoIzquierda.setAttribute("cont", 7 )							

						};
						if (AbajoIzquierdaCont == 5) {

							AbajoIzquierda.setAttribute("cont", 6 )							

						};
						if (AbajoIzquierdaCont == 4) {

							AbajoIzquierda.setAttribute("cont", 5 )							

						};
						if (AbajoIzquierdaCont == 3) {

							AbajoIzquierda.setAttribute("cont", 4 )							

						};
						if (AbajoIzquierdaCont == 2) {

								AbajoIzquierda.setAttribute("cont", 3 )								

						};
						if ( AbajoIzquierdaCont == 1) {

								AbajoIzquierda.setAttribute("cont",2 )								
						};
						if (AbajoIzquierdaCont == 0) {

								AbajoIzquierda.setAttribute("cont", 1)												

						};
				} 
				if (Izquierda != null && resultMulti[x][(y - 1)] != 1) {
							
						IzquierdaCont = Izquierda.getAttribute("cont");

						if (IzquierdaCont == 7) {

							Izquierda.setAttribute("cont", 8 )							

						};
						if (IzquierdaCont == 6) {

							Izquierda.setAttribute("cont", 7 )							

						};
						if (IzquierdaCont == 5) {

							Izquierda.setAttribute("cont", 6 )							

						};
						if (IzquierdaCont == 4) {

							Izquierda.setAttribute("cont", 5 )							

						};
						if (IzquierdaCont == 3) {

							Izquierda.setAttribute("cont", 4 )							

						};
						if (IzquierdaCont == 2) {

								Izquierda.setAttribute("cont", 3 )								

						};
						if ( IzquierdaCont == 1) {

								Izquierda.setAttribute("cont", 2 )								
						};
						if (IzquierdaCont == 0) {

								Izquierda.setAttribute("cont", 1)												

						};			
				} 
				if (ArribaIzquierda != null && resultMulti[(x - 1)][(y - 1)] != 1) {
							
						ArribaIzquierdaCont = ArribaIzquierda.getAttribute("cont");

							
						if (ArribaIzquierdaCont == 7) {

							ArribaIzquierda.setAttribute("cont", 8 )							

						};
						if (ArribaIzquierdaCont == 6) {

							ArribaIzquierda.setAttribute("cont", 7 )							

						};
						if (ArribaIzquierdaCont == 5) {

							ArribaIzquierda.setAttribute("cont", 6 )							

						};
						if (ArribaIzquierdaCont == 4) {

							ArribaIzquierda.setAttribute("cont", 5 )							

						};
						if (ArribaIzquierdaCont == 3) {

							ArribaIzquierda.setAttribute("cont", 4 )							

						};
						if (ArribaIzquierdaCont == 2) {

								ArribaIzquierda.setAttribute("cont", 3 )								

						};
						if ( ArribaIzquierdaCont == 1) {

								ArribaIzquierda.setAttribute("cont", 2 )								
						};
						if (ArribaIzquierdaCont == 0) {

								ArribaIzquierda.setAttribute("cont",1)												

						};


							
							
				}

			}else {



				if (Arriba != null && resultMulti[(x - 1)][y] != 1) {

					ArribaCont = Arriba.getAttribute("cont");
					Arriba.style.background = "#E0E0E0";
					Arriba.removeEventListener("click", click);
					Arriba.removeEventListener("contextmenu", bandera);
					Arriba.setAttribute("clicked", 1);

					if (ArribaCont != 0) {
						Arriba.innerHTML = ArribaCont;
					}
					//console.log(Arriba)

				};
				if (ArribaDerecha != null && resultMulti[(x - 1)][(y + 1 )] != 1) {

					ArribaDerechaCont = ArribaDerecha.getAttribute("cont");
					ArribaDerecha.style.background = "#E0E0E0";
					ArribaDerecha.removeEventListener("click", click);
					ArribaDerecha.removeEventListener("contextmenu", bandera);
					ArribaDerecha.setAttribute("clicked", 1);

					if (ArribaDerechaCont != 0) {
						ArribaDerecha.innerHTML = ArribaDerechaCont;
						
					};
					//console.log(ArribaDerecha)

				};
				if (Derecha != null && resultMulti[x][(y + 1 )] != 1) {

					DerechaCont = Derecha.getAttribute("cont");
					Derecha.style.background = "#E0E0E0";
					Derecha.removeEventListener("click", click);
					Derecha.removeEventListener("contextmenu", bandera);
					Derecha.setAttribute("clicked", 1);

					if (DerechaCont != 0) {
						Derecha.innerHTML = DerechaCont;
						
					};
					//console.log(Derecha)

				};
				if (AbajoDerecha != null && resultMulti[(x + 1)][(y + 1 )] != 1)  {

					AbajoDerechaCont = AbajoDerecha.getAttribute("cont");
					AbajoDerecha.style.background = "#E0E0E0";
					AbajoDerecha.removeEventListener("click", click);
					AbajoDerecha.removeEventListener("contextmenu", bandera);
					AbajoDerecha.setAttribute("clicked", 1);

					if (AbajoDerechaCont != 0) {
						AbajoDerecha.innerHTML = AbajoDerechaCont;
						
					};
					//console.log(AbajoDerecha)

				};
				if (Abajo != null && resultMulti[(x + 1)][y] != 1) {

					AbajoCont = Abajo.getAttribute("cont");
					Abajo.style.background = "#E0E0E0";
					Abajo.removeEventListener("click", click);
					Abajo.removeEventListener("contextmenu", bandera);
					Abajo.setAttribute("clicked", 1);

					if (AbajoCont != 0) {
						Abajo.innerHTML = AbajoCont;
						
					};
					//console.log(Abajo)

				};
				if (AbajoIzquierda != null && resultMulti[(x + 1)][(y - 1)] != 1) {

					AbajoIzquierdaCont = AbajoIzquierda.getAttribute("cont");
					AbajoIzquierda.style.background = "#E0E0E0";
					AbajoIzquierda.removeEventListener("click", click);
					AbajoIzquierda.removeEventListener("contextmenu", bandera);
					AbajoIzquierda.setAttribute("clicked", 1);

					if (AbajoIzquierdaCont != 0) {
						AbajoIzquierda.innerHTML = AbajoIzquierdaCont;
						
					};
					//console.log(AbajoIzquierda)

				};
				if (Izquierda != null && resultMulti[x][(y - 1)] != 1){

					IzquierdaCont = Izquierda.getAttribute("cont");
					Izquierda.style.background = "#E0E0E0";
					Izquierda.removeEventListener("click", click);
					Izquierda.removeEventListener("contextmenu", bandera);
					Izquierda.setAttribute("clicked", 1);

					if (IzquierdaCont != 0) {
						Izquierda.innerHTML = IzquierdaCont;
						
					};
					//console.log(Izquierda)

				};
				if (ArribaIzquierda != null && resultMulti[(x - 1)][(y - 1)] != 1) {

					ArribaIzquierdaCont = ArribaIzquierda.getAttribute("cont");
					ArribaIzquierda.style.background = "#E0E0E0";
					ArribaIzquierda.removeEventListener("click", click);
					ArribaIzquierda.removeEventListener("contextmenu", bandera);
					ArribaIzquierda.setAttribute("clicked", 1);

					if (ArribaIzquierdaCont != 0) {
						ArribaIzquierda.innerHTML = ArribaIzquierdaCont;
						
					};
					//console.log(ArribaIzquierda)

				};

			};	
		}

	
	
}	
